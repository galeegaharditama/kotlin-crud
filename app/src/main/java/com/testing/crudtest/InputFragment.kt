package com.testing.crudtest

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
//import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.testing.crudtest.adapter.NoteDao
import com.testing.crudtest.adapter.NoteRoomDatabase
import com.testing.crudtest.model.Note
import kotlinx.android.synthetic.main.fragment_input.*


class InputFragment : Fragment() {

    private lateinit var note: Note
    private var isUpdate = false
    //private lateinit var database: NoteRoomDatabase
    private lateinit var dao: NoteDao
    private var idstaff = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getData()

        val root = inflater.inflate(R.layout.fragment_input, container, false)


        val btn = root.findViewById<View>(R.id.button_save)
        val btnDelete = root.findViewById<View>(R.id.button_delete)
        btn.setOnClickListener {
            val name = edit_text_name.text.toString()
            val positions = edit_text_positions.text.toString()
            val status = edit_text_status.text.toString()
            val address = edit_text_address.text.toString()

            if (name.isEmpty() && positions.isEmpty() && status.isEmpty() && address.isEmpty()){
                Toast.makeText(context!!, "Data cannot be empty", Toast.LENGTH_SHORT).show()
            }
            else{
                if (isUpdate){
                    saveNote(Note(id = id, name = name, positions = positions, status = status, address = address))
                }
                else{
                    saveNote(Note(name = name, positions = positions, status = status, address = address))
                }
            }
        }

        btnDelete.setOnClickListener {
            dao.deleteById(id)
            Toast.makeText(context!!, "Data removed", Toast.LENGTH_SHORT).show()
        }


        return root

        //return inflater.inflate(R.layout.fragment_input, container, false)
    }

    private fun getData(){

        //database = NoteRoomDatabase.getDatabase(getActivity())
        dao = NoteRoomDatabase.getDatabase(requireContext()).getNoteDao()

        try{
            val bundle = this.arguments
            if (bundle != null) {
                button_delete.visibility = View.VISIBLE
                isUpdate = true

                idstaff = bundle.getInt("MainActId", 0) //0 is the default value , you can change that
                val name = bundle.getString("MainActName", "")
                val positions = bundle.getString("MainActPositions", "")
                val status = bundle.getString("MainActStatus", "")
                val address = bundle.getString("MainActAddress", "")

                edit_text_name.setText(name)
                edit_text_positions.setText(positions)
                edit_text_status.setText(status)
                edit_text_address.setText(address)
            }

            Log.i("load(form edit act)", "id: " + id)
        }catch (ex: Exception){
            Log.i("error exception", "catch " + ex)
        }


    }

    private fun saveNote(note: Note){

        //if (dao.getById(note.id).isEmpty()){
        if(dao.getByName(note.name) != edit_text_name){
            dao.insert(note)
            Toast.makeText(requireContext(), "Data " + note.name + " Saved", Toast.LENGTH_SHORT).show()
        }
        else{
            dao.update(note)
            Toast.makeText(requireContext(), "Data " + note.name + "  Updated", Toast.LENGTH_SHORT).show()
        }
    }

}