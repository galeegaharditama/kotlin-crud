package com.testing.crudtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.testing.crudtest.adapter.SessionManager
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.jetbrains.anko.startActivity

import java.util.*

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // check if session is active
        val currentTime = Calendar.getInstance().time
        val sessionStatus = SessionManager.isSessionActive(currentTime, this)
        val token = SessionManager.getUserToken(this)
        //textToken.text = token
        //textSessionStatus.text = sessionStatus.toString()

        setContentView(R.layout.activity_dashboard)

        btnLihatDataApi.setOnClickListener{
            startActivity<MainActivity>()
        }
        btnAddDataDummy.setOnClickListener {
            startActivity<MaintestActivity>()
        }
        btnLihatDataDummy.setOnClickListener{
            startActivity<InputActivity>()
        }
        btnLogout.setOnClickListener {
            SessionManager.endUserSession(this)
            startActivity<LoginActivity>()
            finish()
        }
    }



}