package com.testing.crudtest


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.testing.crudtest.adapter.NoteDao
import com.testing.crudtest.adapter.NoteRoomDatabase
import com.testing.crudtest.model.Note
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_list.recycler_view_main

class ListFragment : Fragment() {
    //private lateinit var database: NoteRoomDatabase
    private lateinit var dao: NoteDao

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        getNotesData()
        //dao = NoteRoomDatabase.getDatabase(requireContext()).getNoteDao()


        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    private fun getNotesData(){
        val database = NoteRoomDatabase.getDatabase(context!!)
        val dao = database.getNoteDao()
        val listItems = arrayListOf<Note>()
        listItems.addAll(dao.getAll())
        Log.i("load(data dari room)", "data array: " + listItems)
        //setupRecyclerView(listItems)

    }



    override fun onResume() {
        super.onResume()
        getNotesData()
    }
}