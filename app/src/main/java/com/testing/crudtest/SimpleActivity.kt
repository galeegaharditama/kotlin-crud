package com.testing.crudtest

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_crud_simple.*
import com.testing.crudtest.adapter.SimpleBiodataDbManager

class SimpleActivity : AppCompatActivity() {

    private var listDatas = ArrayList<Simple>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crud_simple)
        loadQueryAll()

        lySimples.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            Toast.makeText(this, "Click on " + listDatas[position].name, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.addNote -> {
                    var intent = Intent(this, InputActivity::class.java)
                    startActivity(intent)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        loadQueryAll()
    }

    fun loadQueryAll() {

        var dbManager = SimpleBiodataDbManager(this)
        val cursor = dbManager.queryAll()

        listDatas.clear()
        if (cursor.moveToFirst()) {

            do {
                val id = cursor.getInt(cursor.getColumnIndex("Id"))
                val name = cursor.getString(cursor.getColumnIndex("Name"))
                val birthday = cursor.getString(cursor.getColumnIndex("Birthday"))
                val status = cursor.getString(cursor.getColumnIndex("Status"))
                val address = cursor.getString(cursor.getColumnIndex("Address"))

                listDatas.add(Simple(id, name, birthday, status, address))

            } while (cursor.moveToNext())
        }

        var simplesAdapter = SimpleAdapter(this, listDatas)
        lySimples.adapter = simplesAdapter
    }

    inner class SimpleAdapter : BaseAdapter {

        private var datasList = ArrayList<Simple>()
        private var context: Context? = null

        constructor(context: Context, datasList: ArrayList<Simple>) : super() {
            this.datasList = datasList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.simple, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            var mData = datasList[position]

            vh.lyName.text = mData.name
            vh.lyBirthday.text = mData.birthday
            vh.lyAddress.text = mData.address
            vh.lyStatus.text = mData.status

            vh.ivEdit.setOnClickListener {
                updateData(mData)
            }

            vh.ivDelete.setOnClickListener {
                var dbManager = SimpleBiodataDbManager(this.context!!)
                val selectionArgs = arrayOf(mData.id.toString())
                dbManager.delete("Id=?", selectionArgs)
                loadQueryAll()
            }

            return view
        }

        override fun getItem(position: Int): Any {
            return datasList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return datasList.size
        }
    }

    private fun updateData(simple: Simple) {
        var intent = Intent(this, InputActivity::class.java)
        intent.putExtra("MainActId", simple.id)
        intent.putExtra("MainActName", simple.name)
        intent.putExtra("MainActBirthday", simple.birthday)
        intent.putExtra("MainActStatus", simple.status)
        intent.putExtra("MainActAddress", simple.address)
        Log.i("test update", "update, position: " + simple.id)
        startActivity(intent)
    }

    private class ViewHolder(view: View?) {
        val lyName: TextView
        val lyBirthday: TextView
        val lyStatus: TextView
        val lyAddress: TextView
        val ivEdit: ImageView
        val ivDelete: ImageView

        init {
            this.lyName = view?.findViewById(R.id.lyName) as TextView
            this.lyBirthday = view?.findViewById(R.id.lyBirthday) as TextView
            this.lyStatus = view?.findViewById(R.id.lyStatus) as TextView
            this.lyAddress = view?.findViewById(R.id.lyAddress) as TextView
            this.ivEdit = view?.findViewById(R.id.ivEdit) as ImageView
            this.ivDelete = view?.findViewById(R.id.ivDelete) as ImageView
        }
    }

}