package com.testing.crudtest

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.testing.crudtest.adapter.SimpleBiodataDbManager
import kotlinx.android.synthetic.main.activity_simple.*
import kotlinx.android.synthetic.main.simple.*
import org.jetbrains.anko.startActivity
import java.time.Instant

class InputActivity : AppCompatActivity (){
    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple)

        try{
            val extras = intent.extras
            id = extras!!.getInt("MainActId", 0)
            //var id = extras!!.getInt("MainActId")

            if (id != 0) {
                edtName.setText(extras!!.getString("MainActName"))
                edtBirthday.setText(extras!!.getString("MainActBirthday"))
                edtStatus.setText(extras!!.getString("MainActStatus"))
                edtAddress.setText(extras!!.getString("MainActAddress"))
            }
            Log.i("load(form input)", "id: " + id)
        }catch (ex: Exception){
            Log.i("error exception", "catch " + ex)
        }

        btAdd.setOnClickListener {
            var dbManager = SimpleBiodataDbManager(this)

            var values = ContentValues()
            values.put("Name", edtName.text.toString())
            values.put("Birthday", edtBirthday.text.toString())
            values.put("Status", edtStatus.text.toString())
            values.put("Address", edtAddress.text.toString())
            Log.i("button input", "id: " + id)

            if (id == 0) {
                val mID = dbManager.insert(values)

                if (mID > 0) {
                    Toast.makeText(this, "Add Biodata successfully!", Toast.LENGTH_LONG).show()
                    startActivity<SimpleActivity>()
                } else {
                    Toast.makeText(this, "Failed to add Biodata!", Toast.LENGTH_LONG).show()
                }
            } else {
                var selectionArs = arrayOf(id.toString())
                val mID = dbManager.update(values, "Id=?", selectionArs)

                if (mID > 0) {
                    Toast.makeText(this, "Update Biodata successfully!", Toast.LENGTH_LONG).show()
                    startActivity<SimpleActivity>()
                } else {
                    Toast.makeText(this, "Failed to Update Biodata!", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
