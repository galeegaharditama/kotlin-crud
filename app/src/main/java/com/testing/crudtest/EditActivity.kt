package com.testing.crudtest

import android.os.Bundle
import android.widget.Toast
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.testing.crudtest.adapter.NoteDao
import com.testing.crudtest.adapter.NoteRoomDatabase
import com.testing.crudtest.model.Note
import kotlinx.android.synthetic.main.activity_input.*
import kotlinx.android.synthetic.main.activity_simple.*


class EditActivity : AppCompatActivity() {

    val GET_DATA = "get_data"
    private lateinit var note: Note
    private var isUpdate = false
    private lateinit var database: NoteRoomDatabase
    private lateinit var dao: NoteDao
    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input)

        database = NoteRoomDatabase.getDatabase(applicationContext)
        dao = database.getNoteDao()

        try{
            val extras = intent.extras
            id = extras!!.getInt("MainActId", 0)
            //var id = extras!!.getInt("MainActId")

            if (id != 0) {
                button_delete.visibility = View.VISIBLE
                isUpdate = true

                edit_text_name.setText(extras!!.getString("MainActName"))
                edit_text_positions.setText(extras!!.getString("MainActPositions"))
                edit_text_status.setText(extras!!.getString("MainActStatus"))
                edit_text_address.setText(extras!!.getString("MainActAddress"))
            }
            Log.i("load(form edit act)", "id: " + id)
        }catch (ex: Exception){
            Log.i("error exception", "catch " + ex)
        }

        /*if (intent.getParcelableExtra<Note>(GET_DATA) != null){
            button_delete.visibility = View.VISIBLE
            isUpdate = true
            note = intent.getParcelableExtra(GET_DATA)
            edit_text_name.setText(note.name)
            edit_text_positions.setText(note.positions)
            edit_text_status.setText(note.status)
            edit_text_address.setText(note.address)

            edit_text_name.setSelection(note.name.length)

        }*/

        button_save.setOnClickListener {
            val name = edit_text_name.text.toString()
            val positions = edit_text_positions.text.toString()
            val status = edit_text_status.text.toString()
            val address = edit_text_address.text.toString()

            if (name.isEmpty() && positions.isEmpty() && status.isEmpty() && address.isEmpty()){
                Toast.makeText(applicationContext, "Data cannot be empty", Toast.LENGTH_SHORT).show()
            }
            else{
                if (isUpdate){
                    saveNote(Note(id = id, name = name, positions = positions, status = status, address = address))
                }
                else{
                    saveNote(Note(name = name, positions = positions, status = status, address = address))
                }
            }

            finish()
        }

        button_delete.setOnClickListener {
            dao.deleteById(id)
            Toast.makeText(applicationContext, "Data removed", Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    private fun saveNote(note: Note){

        if (dao.getById(note.id).isEmpty()){

            dao.insert(note)
        }
        else{

            dao.update(note)
        }

        Toast.makeText(applicationContext, "Data saved", Toast.LENGTH_SHORT).show()

    }

    private fun deleteNote(note: Note){
        dao.delete(note)
        Toast.makeText(applicationContext, "Data removed", Toast.LENGTH_SHORT).show()
    }
}
