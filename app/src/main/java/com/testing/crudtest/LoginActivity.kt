package com.testing.crudtest

import android.content.Intent
import android.os.Bundle


import androidx.appcompat.app.AppCompatActivity
import android.util.Log

import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.testing.crudtest.adapter.SessionManager
import java.util.*
import android.view.Window
import android.view.WindowManager
import android.os.Handler
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    var _emailText: EditText? = null
    var _passwordText: EditText? = null
    var _loginButton: TextView? = null


    public override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        val currentTime = Calendar.getInstance().time
        val sessionIsActive = SessionManager.isSessionActive(currentTime, this)

        if (sessionIsActive){
            startActivity(Intent(this, MainsActivity::class.java))
        }
        setContentView(com.testing.crudtest.R.layout.activity_login)

        _loginButton = findViewById(R.id.lin) as TextView
        _passwordText = findViewById(R.id.pswrdd) as EditText
        _emailText = findViewById(R.id.usrusr) as EditText
        _loginButton!!.setOnClickListener { login() }
    }

    fun login() {
        Log.d(TAG, "Login")

        if (!validate()) {
            onLoginFailed()
            return
        }

        _loginButton!!.isEnabled = false


        val email = _emailText!!.text.toString()
        val password = _passwordText!!.text.toString()

        // TODO: Implement your own authentication logic here.

        android.os.Handler().postDelayed(
            {
                // On complete call either onLoginSuccess or onLoginFailed
                onLoginSuccess()
                // onLoginFailed();
                //progressDialog.dismiss()
            }, 3000)


    }



    override fun onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true)
    }

    fun onLoginSuccess() {
        _loginButton!!.isEnabled = true
        // Variable to hold progress status
        var progressStatus = 0;

        // Initialize a new Handler instance
        val handler: Handler = Handler()

        // Start the lengthy operation in a background thread
        Thread(Runnable {
            while (progressStatus < 100) {
                // Update the progress status
                progressStatus += 90

                // Try to sleep the thread for 50 milliseconds
                try {
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                // Update the progress bar
                handler.post(Runnable {
                    progress_bar.progress = progressStatus
                })
            }
        }).start() // Start the operation

        //set session
        SessionManager.startUserSession(this, 60)
        SessionManager.storeUserToken(this, "randomUserToken")
        startActivity(Intent(this, MainsActivity::class.java))
    }

    fun onLoginFailed() {
        Toast.makeText(baseContext, "Login failed", Toast.LENGTH_LONG).show()

        _loginButton!!.isEnabled = true
    }

    fun validate(): Boolean {
        var valid = true

        val email = _emailText!!.text.toString()
        val password = _passwordText!!.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText!!.error = "enter a valid email address"
            valid = false
        } else {
            _emailText!!.error = null
        }

        if (password.isEmpty() || password.length < 4 || password.length > 10) {
            _passwordText!!.error = "between 4 and 10 alphanumeric characters"
            valid = false
        } else {
            _passwordText!!.error = null
        }

        return valid
    }

    companion object {
        private val TAG = "LoginActivity"
    }
}
