package com.testing.crudtest

class Simple {

    var id: Int? = null
    var name: String? = null
    var birthday: String? = null
    var status: String? = null
    var address: String? = null

    constructor(id: Int, name: String, birthday: String, status: String, address: String) {
        this.id = id
        this.name = name
        this.birthday = birthday
        this.status = status
        this.address = address
    }

}
